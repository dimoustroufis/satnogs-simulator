# satnogs-simulator: A simulation tool for the SatNOGS network

The satnogs-simulator is a CLI based tool, that simulates the SatNOGS network with
orbiting satellites.

The simulation parameters are specified through a YAML file.
Such a file can be found at the `contrib` directory.

Users can specify:
* The resolution of the simulation in miliseconds
* The start and stop date
* The tool makes use of a thread pool. Its size is also configurable
* Satellite information can be retrieved from the SatNOGS-DB. Custom
satellites are also supported by specifying the name and the TLE of the custom entry
* Ground station information can be retrived automatically from the SatNOGS network.
Again, custom ground stations can be defined too
* The simulation supports a basic transmission scheme for each satellite.
Messages are randommly "transmitted". Those messages are delivered to stations that
have a line of sight with the corresponding satellite. The line of site depends on the
ground station configured minimum horizon parameter.
* A set of events that can occur on the satellite while orbiting, are also supported.
Currently random reboots and reboots caused by the South-Atlantic-Anomaly are implemented

## Requirements
* CMake (>= 3.20)
* C++17
* GNU Make
* spdlog
* cpprest
* boost-random
* yaml-cpp

## Performance
The architecture of the simulation tool has been optimized for perfomance and speed
trying to reduce the critical sections between trheads.

> [!WARNING]
> There is a known issue with the REST API of SatNOGS, being to aggressive on the throttling.
> This will delay considerably the fetching of satellites and especially the ground stations.


## License
![Libre Space Foundation](docs/assets/LSF_HD_Horizontal_Color1-300x66.png)
&copy; 2024 [Libre Space Foundation](https://libre.space).

