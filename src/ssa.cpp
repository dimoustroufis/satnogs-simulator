/*
 *  satnogs-simulator: Satellite and Ground station network simulation tool
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "ssa.hpp"
#include <iostream>

ssa::ssa(const YAML::Node &config)
    : m_enabled(
          config["events"]["south-atlantic-anomaly"]["enable"].as<bool>()),
      m_prob(config["events"]["south-atlantic-anomaly"]["reset-prob"]
                 .as<double>()),
      m_max_alt(
          config["events"]["south-atlantic-anomaly"]["max-alt"].as<double>())
{
  auto &coords = config["events"]["south-atlantic-anomaly"]["area"];

  for (const auto &i : coords) {
    auto                      p = i.begin();
    std::pair<double, double> x;
    x.first = p->as<double>();
    p++;
    x.second = p->as<double>();
    m_polygon.push_back(x);
  }
}

bool
ssa::affected(const std::pair<double, double> &x, double alt) const
{
  if (!m_enabled || alt > m_max_alt) {
    return false;
  }

  size_t i;
  size_t j;
  size_t cnt = 0;
  for (i = 0, j = m_polygon.size() - 1; i < m_polygon.size(); j = i++) {
    if (((m_polygon[i].second > x.second) !=
         (m_polygon[j].second > x.second)) &&
        (x.first < (m_polygon[j].first - m_polygon[i].first) *
                           (x.second - m_polygon[i].second) /
                           (m_polygon[j].second - m_polygon[i].second) +
                       m_polygon[i].first)) {
      cnt++;
    }
  }
  return cnt % 2 == 1;
}

double
ssa::prob() const
{
  return m_prob;
}
