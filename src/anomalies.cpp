#include "anomalies.hpp"

anomalies::anomalies(const YAML::Node &config)
    : m_reset_prob(config["events"]["random-resets-prob"].as<double>()),
      m_ssa(config)
{
}

const ssa &
anomalies::get_ssa() const
{
  return m_ssa;
}

double
anomalies::reset_prob() const
{
  return m_reset_prob;
}
