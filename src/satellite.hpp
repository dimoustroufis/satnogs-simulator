/*
 *  satnogs-simulator: Satellite and Ground station network simulation tool
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once
#include "anomalies.hpp"
#include "link.h"
#include "telemetry.hpp"
#include <iostream>
#include <libsgp4/CoordGeodetic.h>
#include <libsgp4/CoordTopocentric.h>
#include <libsgp4/Observer.h>
#include <libsgp4/SGP4.h>
#include <random>
#include <string>
#include <yaml-cpp/yaml.h>

class sat_link;

class satellite
{
public:
  satellite(const std::string &name, const std::string &tle, const anomalies &a,
            double tlm_prob);

  const std::string &
  tle() const;

  std::string
  tle(size_t line) const;

  const std::string &
  name() const;

  friend std::ostream &
  operator<<(std::ostream &output, const satellite &S)
  {
    output << "Name        : " << S.m_name << std::endl;
    output << "TLE         : " << S.m_tle << std::endl;
    output << "Telemetry   : " << std::endl << S.tlm() << std::endl;
    return output;
  }

  void
  update(const DateTime &t);

  Eci
  position() const;

  const telemetry &
  tlm() const;

  void
  register_link(std::shared_ptr<sat_link> l);

private:
  const std::string  m_name;
  const std::string  m_tle;
  bool               m_first_update;
  DateTime           m_timestamp;
  DateTime           m_persec_timestamp;
  SGP4               m_sgp4;
  Eci                m_eci;
  telemetry          m_tlm;
  const anomalies   &m_anomalies;
  std::random_device m_rd;
  /* No the best random, but we primarly care about speed */
  std::minstd_rand0                      m_gen;
  std::bernoulli_distribution            m_reset_dist;
  std::bernoulli_distribution            m_ssa_dist;
  std::bernoulli_distribution            m_tlm_dist;
  std::vector<std::shared_ptr<sat_link>> m_links;
};
