/*
 *  satnogs-simulator: Satellite and Ground station network simulation tool
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "simulation.hpp"
#include "sat_link.hpp"
#include <chrono>
#include <cpprest/filestream.h>
#include <cpprest/http_client.h>
#include <cpprest/json.h>
#include <cpprest/uri.h>
#include <future>
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/spdlog.h>

using namespace utility;
using namespace web;
using namespace web::http;
using namespace web::http::client;
using namespace concurrency::streams;

simulation::simulation(const YAML::Node &config)
    : m_config(config),
      m_max_threads(config["simulation"]["threads-num"].as<size_t>()),
      m_resolution_ms(config["simulation"]["resolution-ms"].as<size_t>()),
      m_anomalies(config)
{
  create_stations(config);
  create_sats(config);
  create_links();

  m_start_time =
      parse_iso8601_UTC(config["simulation"]["start"].as<std::string>());
  m_stop_time =
      parse_iso8601_UTC(config["simulation"]["stop"].as<std::string>());
  print();

  m_spdlog_handlers.after_open = [](spdlog::filename_t filename,
                                    std::FILE         *fstream) {
    fputs("Date, Satellite, Satellite Azimuth, Satellite Elevation, Satellite "
          "Range, Satellite Uptime (ms), Satellite Resets, Ground Station\n",
          fstream);
  };
}

void
simulation::start()
{
  auto logger = spdlog::basic_logger_mt(
      "file_logger", m_config["simulation"]["reception-log"].as<std::string>(),
      false, m_spdlog_handlers);
  logger->set_pattern("%v");
  std::chrono::time_point<std::chrono::steady_clock> tic =
      std::chrono::steady_clock::now();
  m_cur_time = m_start_time;
  while (m_cur_time < m_stop_time) {
    /* Assign all possible sattelites to async tasks */
    std::vector<std::future<void>> tasks;
    if (m_sats.size() / m_max_threads) {
      size_t idx = 0;
      for (size_t i = 0; i < m_max_threads - 1; i++) {
        auto t = std::async(std::launch::async, &simulation::run, this, idx,
                            m_sats.size() / m_max_threads);
        tasks.emplace_back(std::move(t));
        idx += m_sats.size() / m_max_threads;
      }
      auto t = std::async(std::launch::async, &simulation::run, this, idx,
                          m_sats.size() - idx);
      tasks.emplace_back(std::move(t));

    } else {
      auto t = std::async(std::launch::async, &simulation::run, this, 0,
                          m_sats.size());
      tasks.emplace_back(std::move(t));
    }
    for (auto &i : tasks) {
      i.wait();
    }

    /* Update the statistics of GS */
    for (auto &i : m_gs) {
      i.update(m_cur_time, logger);
    }
    m_cur_time = m_cur_time.AddMicroseconds(m_resolution_ms * 1000);
  }

  std::chrono::time_point<std::chrono::steady_clock> toc =
      std::chrono::steady_clock::now();
  std::cout
      << "Simulation executed in "
      << std::chrono::duration_cast<std::chrono::seconds>(toc - tic).count()
      << " seconds" << std::endl;
  results();
}

void
simulation::results()
{
  std::cout << "============ GS stats ============" << std::endl;
  for (const auto &i : m_gs) {
    i.print_results();
  }
  std::cout << "==================================" << std::endl;

  std::cout << "============ Satellite stats ============" << std::endl;
  for (const auto &[key, val] : m_sats) {
    std::cout << val << std::endl;
  }
  std::cout << "==================================" << std::endl;
}

void
simulation::create_stations(const YAML::Node &config)
{
  bool has_more = true;
  auto uri =
      config["ground-stations"]["satnogs"]["network-url"].as<std::string>() +
      "/api/stations";

  while (has_more) {
    auto         client = http_client(uri);
    http_request request(methods::GET);
    request.headers().add(
        U("Authorization"),
        U("Token ") +
            config["ground-stations"]["satnogs"]["key"].as<std::string>());

    /* Query the SatNOGS network if this observation was reported with no signal */

    auto request_json =
        client.request(methods::GET)
            .then([&](http_response response) {
              if (response.status_code() != status_codes::OK) {
                throw std::runtime_error(
                    "Returned " + std::to_string(response.status_code()));
              }

              /*
               * Find if there is a next page of SatNOGS stations.
               * It is not the most efficient and friendly design...
               */
              auto link = response.headers()["Link"];
              if (link.find("rel=\"next\"") != link.npos) {
                uri = link.substr(1, link.find(">; rel=\"next\"") - 1);
              } else {
                has_more = false;
              }

              return response.extract_json();
            })
            .then([&](json::value obj) {
              for (auto &s : obj.as_array()) {
                if ((config["ground-stations"]["satnogs"]["include-online"]
                         .as<bool>() &&
                     s["status"].as_string() == "Online") ||
                    (config["ground-stations"]["satnogs"]["include-offline"]
                         .as<bool>() &&
                     s["status"].as_string() == "Offline") ||
                    (config["ground-stations"]["satnogs"]["include-testing"]
                         .as<bool>() &&
                     s["status"].as_string() == "Testing")) {
                  m_gs.push_back(gs(s["name"].as_string(), s["lat"].as_double(),
                                    s["lng"].as_double(),
                                    s["altitude"].as_double(),
                                    s["min_horizon"].as_double()));
                }
              }
            });
    request_json.wait();
  }

  for (const auto &i : config["ground-stations"]["custom"]) {
    m_gs.push_back(gs(i["name"].as<std::string>(), i["latitude"].as<double>(),
                      i["longitude"].as<double>(), i["elevation"].as<double>(),
                      i["min_horizon"].as<double>()));
  }
}

const std::string
get_satellite_name(size_t norad, const YAML::Node &config)
{
  auto client =
      http_client(config["satellites"]["satnogs"]["db-url"].as<std::string>());
  http_request request(methods::GET);
  std::string  name;
  auto         request_json =
      client
          .request(methods::GET,
                   uri_builder(U("api"))
                       .append_path(U("satellites"))
                       .append_query("norad_cat_id=" + std::to_string(norad))
                       .to_string())
          .then([&](http_response response) {
            if (response.status_code() != status_codes::OK) {
              throw std::runtime_error("Returned " +
                                       std::to_string(response.status_code()));
            }
            return response.extract_json();
          })
          .then([&](json::value obj) { name = obj[0]["name"].as_string(); });
  request_json.wait();
  return name;
}

const std::string
get_satellite_tle(size_t norad, const YAML::Node &config)
{
  auto client =
      http_client(config["satellites"]["satnogs"]["db-url"].as<std::string>());
  http_request request(methods::GET);

  std::string tle;
  auto        request_json =
      client
          .request(methods::GET,
                   uri_builder(U("api"))
                       .append_path(U("tle"))
                       .append_query("norad_cat_id=" + std::to_string(norad))
                       .to_string())
          .then([&](http_response response) {
            if (response.status_code() != status_codes::OK) {
              throw std::runtime_error("Returned " +
                                       std::to_string(response.status_code()));
            }
            return response.extract_json();
          })
          .then([&](json::value obj) {
            tle = obj[0]["tle1"].as_string();
            tle.append("\n");
            tle += obj[0]["tle2"].as_string();
          });
  request_json.wait();
  return tle;
}

void
simulation::create_sats(const YAML::Node &config)
{
  const auto &ids = config["satellites"]["satnogs"]["norad-ids"];
  for (auto it = ids.begin(); it != ids.end(); it++) {
    try {
      const auto name = get_satellite_name(it->as<size_t>(), config);
      const auto tle  = get_satellite_tle(it->as<size_t>(), config);
      m_sats.try_emplace(name, name, tle, m_anomalies,
                         config["satellites"]["tlm-prob"].as<double>());
    } catch (...) {
    }
  }

  for (const auto &i : config["satellites"]["custom"]) {
    m_sats.try_emplace(i["name"].as<std::string>(), i["name"].as<std::string>(),
                       i["tle"].as<std::string>(), m_anomalies,
                       config["satellites"]["tlm-prob"].as<double>());
  }
}

void
simulation::create_links()
{
  for (auto &i : m_gs) {
    for (auto &[key, val] : m_sats) {
      m_links.push_back(sat_link::sptr(new sat_link(i, val)));
      /*
       * Assign also this link to the GS so we can easily fetch the
       * links assosiated with this particular GS
       */
      i.register_link(m_links[m_links.size() - 1]);
      val.register_link(m_links[m_links.size() - 1]);
    }
  }
}

DateTime
simulation::parse_iso8601_UTC(const std::string &date)
{
  std::tm tm;
  strptime(date.c_str(), "%Y-%m-%dT%H:%M:%S", &tm);
  return DateTime(tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour,
                  tm.tm_min, tm.tm_sec);
}

void
simulation::print()
{
  std::cout << "SatNOGS Simulator" << std::endl;
  std::cout << "Start time (UTC)  : " << m_start_time << std::endl;
  std::cout << "Stop time (UTC)   : " << m_stop_time << std::endl;
  std::cout << "Resolution (ms)   : " << m_resolution_ms << std::endl;
  std::cout << "Ground Stations   : " << m_gs.size() << std::endl;
  std::cout << "Satellites        : " << m_sats.size() << std::endl;
  std::cout << "Potential links   : " << m_links.size() << std::endl;
}

void
simulation::run(size_t idx, size_t n)
{
  auto iter = m_sats.begin();

  for (size_t i = 0; i < idx; i++) {
    iter++;
  }

  for (size_t i = 0; i < n; i++) {
    iter->second.update(m_cur_time);
    iter++;
  }
}
