/*
 *  satnogs-simulator: Satellite and Ground station network simulation tool
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "telemetry.hpp"
#include <random>

telemetry::telemetry() : m_uptime_us(0), m_reset_cnt()
{
  std::random_device                      rd;
  std::mt19937_64                         gen(rd());
  std::uniform_int_distribution<uint64_t> dist_uptime(0, 10000000000);
  std::uniform_int_distribution<uint64_t> dist_rst(0, 100);
  m_uptime_us = dist_uptime(gen);
  m_reset_cnt = dist_rst(gen);
}

uint64_t
telemetry::uptime_ms() const
{
  return m_uptime_us / 1000;
}

size_t
telemetry::reset_cnt() const
{
  return m_reset_cnt;
}

void
telemetry::reset()
{
  m_uptime_us = 0;
  m_reset_cnt++;
}

void
telemetry::update_uptime(size_t us)
{
  m_uptime_us += us;
}
