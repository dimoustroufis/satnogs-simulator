/*
 *  satnogs-simulator: Satellite and Ground station network simulation tool
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "telemetry.hpp"
#include <libsgp4/SGP4.h>
#include <memory>

class msg
{
public:
  using sptr = std::shared_ptr<msg>;

  msg(const DateTime &t, const Eci &pos, const telemetry &tlm);

  const DateTime &
  timestamp() const;

  const Eci &
  position() const;

  const telemetry &
  tlm() const;

private:
  const DateTime  m_t;
  const Eci       m_eci;
  const telemetry m_tlm;
};