/*
 *  satnogs-simulator: Satellite and Ground station network simulation tool
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "satellite.hpp"
#include "sat_link.hpp"

satellite::satellite(const std::string &name, const std::string &tle,
                     const anomalies &a, double tlm_prob)
    : m_name(name),
      m_tle(tle),
      m_first_update(true),
      m_sgp4({name, tle.substr(0, 69), tle.substr(69 + 1, 69)}),
      m_eci(DateTime(), 0, 0, 0),
      m_anomalies(a),
      m_rd(),
      m_gen(m_rd()),
      m_reset_dist(a.reset_prob()),
      m_ssa_dist(a.get_ssa().prob()),
      m_tlm_dist(tlm_prob)
{
}

const std::string &
satellite::tle() const
{
  return m_tle;
}

std::string
satellite::tle(size_t line) const
{
  if (line > 2 || line < 1) {
    throw std::invalid_argument("Wrong number of line. [1,2] allowed");
  }
  if (line == 1) {
    return m_tle.substr(0, 69);
  }
  return m_tle.substr(69 + 1, 69);
}

const std::string &
satellite::name() const
{
  return m_name;
}

void
satellite::update(const DateTime &t)
{
  if (m_first_update) {
    m_timestamp        = t;
    m_persec_timestamp = t;
    m_first_update     = false;
    m_eci              = m_sgp4.FindPosition(t);
  } else {

    /* Model the anomalies once per second */
    auto secs = (t - m_persec_timestamp).TotalSeconds();
    while (secs > 0) {
      m_tlm.update_uptime(1000000);
      m_timestamp        = m_timestamp.AddSeconds(1);
      m_persec_timestamp = m_persec_timestamp.AddSeconds(1);
      m_eci              = m_sgp4.FindPosition(m_timestamp);

      /* Model anomalies from SSA */
      if (m_anomalies.get_ssa().affected(
              {Util::RadiansToDegrees(m_eci.ToGeodetic().latitude),
               Util::RadiansToDegrees(m_eci.ToGeodetic().longitude)},
              m_eci.ToGeodetic().altitude * 1000)) {
        if (m_ssa_dist(m_gen)) {
          m_tlm.reset();
        }
      }

      /* Model resets */
      if (m_reset_dist(m_gen)) {
        m_tlm.reset();
      }

      /* Model telemetry transmissions. Messages will be delievered only is there is AOS */
      if (m_tlm_dist(m_gen)) {
        for (auto i : m_links) {
          i->downlink(msg::sptr(new msg(m_timestamp, m_eci, m_tlm)));
        }
      }
      secs--;
    }
    m_tlm.update_uptime((t - m_timestamp).TotalMicroseconds());
    m_timestamp = t;
  }
}

Eci
satellite::position() const
{
  return m_eci;
}

const telemetry &
satellite::tlm() const
{
  return m_tlm;
}

void
satellite::register_link(sat_link::sptr l)
{
  m_links.push_back(l);
}
