/*
 *  satnogs-simulator: Satellite and Ground station network simulation tool
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once
#include <cstddef>
#include <cstdint>
#include <iostream>

class telemetry
{
public:
  telemetry();

  uint64_t
  uptime_ms() const;

  size_t
  reset_cnt() const;

  void
  reset();

  void
  update_uptime(size_t ms);

  friend std::ostream &
  operator<<(std::ostream &output, const telemetry &tlm)
  {
    output << "Uptime (ms)  : " << tlm.m_uptime_us / 1000 << std::endl;
    output << "Resets       : " << tlm.m_reset_cnt << std::endl;
    return output;
  }

private:
  uint64_t m_uptime_us;
  size_t   m_reset_cnt;
};
