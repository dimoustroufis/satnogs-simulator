/*
 *  satnogs-simulator: Satellite and Ground station network simulation tool
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "msg.hpp"

msg::msg(const DateTime &t, const Eci &pos, const telemetry &tlm)
    : m_t(t), m_eci(pos), m_tlm(tlm)
{
}

const DateTime &
msg::timestamp() const
{
  return m_t;
}

const Eci &
msg::position() const
{
  return m_eci;
}

const telemetry &
msg::tlm() const
{
  return m_tlm;
}
